package xDevs.skystats.pvp;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;

import xDevs.skystats.SkyStats;

public class PVPMySQL {

	public static void updatePVPStat(String stat_name, Player p, String type) 
	{
		if (hasStatistic(stat_name, p, type)) 
		{
			try {
				PreparedStatement ps = SkyStats.con.prepareStatement("UPDATE `"
						+ SkyStats.sqlPrefix + "_PVP"
						+ "` SET "+stat_name+"=? WHERE player_uuid=?");

				System.out.println("Updating value to - " + getPVPStat(stat_name, p, type));
				ps.setInt(1, getPVPStat(stat_name, p, type)+1);
				ps.setString(2, p.getUniqueId().toString());
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				System.out
				.println("[SkyStats] has encountered an issue! Please report this to the devs!");
				e.printStackTrace();
			}
		} else {
			try {
				PreparedStatement ps = SkyStats.con
						.prepareStatement("INSERT INTO " + SkyStats.sqlPrefix
								+ "_PVP" + " VALUES (?,?,?)");

				ps.setString(1, p.getUniqueId().toString());

				if(type.equals("KILLS"))
				{
					ps.setInt(2, 1);
					ps.setInt(3, 0);
				}else{
					ps.setInt(2, 0);
					ps.setInt(3, 1);
				}

				ps.execute();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static Integer getPVPStat(String stat_name, Player p, String type) 
	{
		try {
			PreparedStatement ps = SkyStats.con
					.prepareStatement("SELECT * FROM " + SkyStats.sqlPrefix
							+ "_PVP WHERE player_uuid=?");
			ps.setString(1, p.getUniqueId().toString());
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) 
			{
				while (rs.next()) 
				{
					return rs.getInt(stat_name);
				}
			}
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			System.out
			.println("[SkyStats] has encountered an issue! Please report this to the devs!");
			e.printStackTrace();
		}
		return -1;
	}

	private static Boolean hasStatistic(String stat_name, Player p, String type)
	{
		try {
			PreparedStatement ps = SkyStats.con.prepareStatement("SELECT * FROM " + SkyStats.sqlPrefix + "_PVP WHERE player_uuid=?");

			ps.setString(1, p.getUniqueId().toString());

			ResultSet rs = ps.executeQuery();

			System.out.println(rs.toString());
			if(rs.isBeforeFirst())
			{
				return true;
			}

			ps.execute();
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	

		return false;
	}
}
