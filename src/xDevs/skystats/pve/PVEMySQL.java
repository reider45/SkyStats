package xDevs.skystats.pve;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;

import xDevs.skystats.SkyStats;

public class PVEMySQL {
	
	public static void updatePVEStat(String stat_name, Player p) 
	{
		if (getPVEStat(p) != -1) 
		{
			try {
				PreparedStatement ps = SkyStats.con.prepareStatement("UPDATE `"
						+ SkyStats.sqlPrefix + "_PVE"
						+ "` SET blocks_mined=? WHERE player_uuid=?");

				ps.setInt(1, getPVEStat(p)+1);
				ps.setString(2, p.getUniqueId().toString());
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				System.out
						.println("[SkyStats] has encountered an issue! Please report this to the developer!");
				e.printStackTrace();
			}
		} else {
			try {
				PreparedStatement ps = SkyStats.con
						.prepareStatement("INSERT INTO `" + SkyStats.sqlPrefix
								+ "_PVE` VALUES (?,?)");

				ps.setString(1, p.getUniqueId().toString());
				ps.setInt(2, 1);
				
				ps.execute();
				ps.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static Integer getPVEStat(Player p) 
	{
		try {
			PreparedStatement ps = SkyStats.con
					.prepareStatement("SELECT * FROM " + SkyStats.sqlPrefix
							+ "_PVE WHERE player_uuid=?");
			ps.setString(1, p.getUniqueId().toString());
			ResultSet rs = ps.executeQuery();
			if (rs.isBeforeFirst()) 
			{
				while (rs.next()) 
				{
					return rs.getInt("blocks_mined");
				}
			}
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			System.out
					.println("[SkyStats] has encountered an issue! Please report this to the devs!");
			e.printStackTrace();
		}
		return -1;
	}

}
