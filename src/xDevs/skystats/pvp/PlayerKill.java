package xDevs.skystats.pvp;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;


public class PlayerKill implements Listener {

	@EventHandler
	public void onKill(PlayerDeathEvent e)
	{
		if(e.getEntity().getKiller() instanceof Player)
		{
			Player killer = (Player) e.getEntity().getKiller();
			
			PVPMySQL.updatePVPStat("player_kills", killer, "KILLS"); // +1 Kill
			PVPMySQL.updatePVPStat("player_deaths", e.getEntity(), "DEATHS"); // +1 Death
		}
	}
	
}
