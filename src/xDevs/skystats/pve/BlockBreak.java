package xDevs.skystats.pve;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent e)
	{
		PVEMySQL.updatePVEStat("MinedBlocks", e.getPlayer());
	}

}
