package xDevs.skystats;

import java.sql.Connection; 
import java.sql.DriverManager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import xDevs.skystats.pve.BlockBreak;
import xDevs.skystats.pvp.PlayerKill;

public class SkyStats extends JavaPlugin {

	private static SkyStats plugin;

	public static Connection con;
	public static String sqlPrefix;

	public void onEnable() 
	{
		plugin = this;

		saveDefaultConfig();
		
		createConnection(getConfig().getString("Database.Host"), getConfig()
				.getString("Database.User"),
				getConfig().getString("Database.Password"),
				getConfig().getInt("Database.Port"), getConfig().getString("Database.DBName"));
		
		sqlPrefix = getConfig().getString("Prefix");

		Bukkit.getPluginManager().registerEvents(new PlayerKill(), this);
		Bukkit.getPluginManager().registerEvents(new BlockBreak(), this);
	}

	public void onDisable() 
	{
		plugin = null;
	}

	public static SkyStats getPlugin() 
	{
		return plugin;
	}

	protected Connection createConnection(String host, String user,
			String pass, Integer port, String dbName) 
	{
		try {
			System.out.println("[SkyStats] Connecting to server database!");
			Connection connection = DriverManager.getConnection("jdbc:mysql://"
					+ host + ":" + port + "/" + dbName, user, pass);
			con = connection;
			System.out.println("[SkyStats] Connected to mySQL!");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
